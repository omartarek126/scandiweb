# Scandiweb Junior Developer Test Task
---
## Technologies used:
**1-Frontend:** React: (react, react-dom, react-router-dom) & CSS <br/>
**2-Backend:** PHP plain classes with an OOP approach
---
## Development process:
This project has been a great learning journey for me, since I read that using React was a huge advantage, I started learning it from scratch and used it to develop this website which was a long but a very beneficial and enjoyable experience.
Also, it was a great opportunity to revise on OOP in PHP and learning php PSR coding best practices and conventions.
---
## Deployment:
**1-Frontend:** Firebase <br/>
**2-Backend:** 000webhost